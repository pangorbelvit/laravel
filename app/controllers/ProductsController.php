<?php

use Illuminate\Support\Facades\Input;

class ProductsController extends BaseController {

    public function showIndex()
    {

        $default_sort = 'name';
        $default_order = 'asc';
        $default_count = '20';

        if (Input::get('count') && is_int(intval(Input::get('count')))) {
            $count = Input::get('count');
        } else {
            $count = $default_count;
        }

        if (Input::get('sortby')) {
            switch (Input::get('sortby')) {
                case 'name':
                    $sortby = 'name';
                    break;
                case 'price':
                    $sortby = 'price';
                    break;
                case 'year':
                    $sortby = 'year';
                    break;
                default:
                    $sortby = 'name';
                    break;
            }
        } else {
            $sortby = $default_sort;
        }

        if (Input::get('order') && Input::get('order') == 'desc') {
            $order = 'desc';
        } else {
            $order = $default_order;
        }

        if (Input::get('page') && is_int(intval(Input::get('page')))) {
            $page = Input::get('page');
        } else {
            $page = 1;
        }
        $products = Products::orderBy($sortby, $order)->paginate($count);

        if ($sortby != $default_sort) {
            $data['sortby'] = $sortby;
        } else {
            $data['sortby'] = '';
        }

        if ($order != $default_order) {
            $data['order'] = $order;
        } else {
            $data['order'] = '';
        }

        if ($count != $default_count) {
            $data['count'] = $count;
        } else {
            $data['count'] = '';
        }

        $data['page'] = $page;
        $data['products'] = $products->appends(Input::except('page'));
        return \View::make('products', $data);
    }

}