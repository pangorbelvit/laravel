<?php
use Carbon\Carbon;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ProductTableSeeder');
	}

}

class ProductTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $insert = array();
        for ($i = 0; $i <= 100; $i++) {
            $insert[] = array(
                'name' =>  $faker->name,
                'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
                'description' => $faker->text,
                'year' => $faker->year($max = '-1 years'),
                'created_at' => $faker->dateTimeBetween($startDate = '-1 years',  $endDate = 'now',  $timezone = null),
                'updated_at' => Carbon::now(),
            );
        }
        DB::table('products')->insert($insert);
    }

}
