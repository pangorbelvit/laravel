<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <title>Obyava - main page</title>
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-2 col-sm-2">
                    <a href="/" class="logo"><img src="{{ asset('img/Logo.png') }}" alt=""></a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 search-block">
                    <div class="users">
                        <img src="{{ asset('img/Friends.png') }}" alt="">
                    </div>
                    <div class="search" >
                        <input type="text" placeholder="Поиск">
                    </div>
                    <div class="area">
                        <img src="{{ asset('img/mark.png') }}" alt="">
                        Регион
                    </div>
                    <div class="button">
                        <a href="/" class="add-search">
                            <img src="{{ asset('img/strelka.png') }}" alt="" >
                        </a>
                    </div>
                </div>
                <div class="col-lg-3  col-md-3 col-sm-2 customer-block">
                    <div class="customer">
                        <img src="{{ asset('img/user.png') }}" alt="">
                        <span>
                            Профиль
                        </span>
                        <img src="{{ asset('img/down.png') }}" alt="">
                    </div>
                </div>
            </div>

        </div>
    </header>
    <section class="content">
        <div class="container">
            <div class="top">
                <h1>Портал объявлений</h1>
                <a href="/" class="add">
                    <img src="{{ asset('img/strelka.png') }}" alt="" >
                    <span>Разместить объявление</span>
                </a>
            </div>

        </div>
        <div class="container main-container">
            <div class="row">
                <div class="col-lg-3  col-md-3  category">
                    <a href="/"><img src="{{ asset('img/1.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-lg-3  col-md-3  category">
                    <a href="/"><img src="{{ asset('img/2.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-lg-3  col-md-3  category">
                    <a href="/"><img src="{{ asset('img/3.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-lg-3  col-md-3  category">
                    <a href="/"><img src="{{ asset('img/4.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-lg-3  col-md-3  category">
                    <a href="/"><img src="{{ asset('img/5.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/1.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/2.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/3.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/4.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/5.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/1.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/2.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/3.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/4.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
                <div class="col-3 category">
                    <a href="/"><img src="{{ asset('img/5.png') }}" alt="" class="category-img"></a>
                    <div class="category-name">
                        Недвижимость
                    </div>
                    <div class="category-count">
                        (1234)
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row"> <div class="col-3"><h2>Хотите продать?</h2></div>
                <div class="col-6">
                    В интернете есть много площадок для размещения объявлений. Но до сих пор не было такой удобной. Инновации, удобство и простота - это то, чем отличается портал Obyava.ua. </div>
                <div class="col-3"><a href="/" class="add-border">
                        <img src="{{ asset('img/strelka.png') }}" alt="" >
                        <span>Разместить объявление</span>
                    </a>
                </div>
            </div>

        </div>
    </section>
    <footer>
        <div class="container">
            <a href="/" class="logo"><img src="{{ asset('img/Logo.png') }}" alt=""></a>
                <a href="/" class="footer-link">About us</a>
                <a href="/" class="footer-link">Mobile website</a>
                <a href="/" class="footer-link">FAQ</a>
                <a href="/" class="footer-link">Paid servicies</a>
                <a href="/" class="footer-link">Terms and conditions</a>
                <a href="/" class="footer-link">Help</a>
        </div>
    </footer>
</body>
</html>