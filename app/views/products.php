<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').change(function(){
                var id = $(this).attr('id');
                location.href =  $('#' + id + ' option:selected').data('url');
            })
        });
    </script>
    <style type="text/css">

        td {
            border: 1px solid black;
            text-align: center;
        }
        table {
            border: 1px solid black;
            width: 100%;
        }
    </style>
</head>
<body>
<?php
$params_url = array();
if ($count) {
    $params_url['count'] = $count;
}
if ($sortby) {
    $params_url['sortby'] = $sortby;
}
if ($order) {
    $params_url['order'] = $order;
}
?>

<?php echo $products->links(); ?>
<div>
    <label>
        Количество товаров на траницу
        <select id="count">
            <option value="10" data-url="<?php $new_param = $params_url; $new_param['count'] = 10; echo action('ProductsController@showIndex', $new_param); ?>" <?php if ($count == 10) {?> selected <?php } ?>>10</option>
            <option value="20"  data-url="<?php $new_param = $params_url; unset($new_param['count']); echo action('ProductsController@showIndex', $new_param); ?>" <?php if (!$count) {?> selected <?php } ?>>20</option>
            <option value="50"  data-url="<?php $new_param = $params_url; $new_param['count'] = 50; echo action('ProductsController@showIndex', $new_param); ?>"  <?php if ($count == 50) {?> selected <?php } ?>>50</option>
        </select>
    </label>
</div>
<div>
    <label>
        Сотрировка товаров
        <select id="sortby">
            <option value="name" data-url="<?php $new_param = $params_url; unset($new_param['sortby']); unset($new_param['order']);  echo action('ProductsController@showIndex', $new_param); ?>"<?php if (!$sortby && !$order) {?> selected <?php } ?>>По имени (а-я)</option>
            <option value="name" data-url="<?php $new_param = $params_url; unset($new_param['sortby']); $new_param['order'] = 'desc';  echo action('ProductsController@showIndex', $new_param); ?>"<?php if (!$sortby && $order == 'desc') {?> selected <?php } ?>>По имени (я-а)</option>
            <option value="price" data-url="<?php $new_param = $params_url; $new_param['sortby'] = 'price';  unset($new_param['order']);  echo action('ProductsController@showIndex', $new_param); ?>"<?php if ($sortby == 'price' && !$order) {?> selected <?php } ?>>По цене (от меньшего к большему)</option>
            <option value="price" data-url="<?php $new_param = $params_url; $new_param['sortby'] = 'price';  $new_param['order'] = 'desc';  echo action('ProductsController@showIndex', $new_param); ?>"<?php if ($sortby == 'price' && $order == 'desc') {?> selected <?php } ?>>По цене (от большего к меньшему)</option>
            <option value="year" data-url="<?php $new_param = $params_url; $new_param['sortby'] = 'year';  unset($new_param['order']);  echo action('ProductsController@showIndex', $new_param); ?>"<?php if ($sortby == 'year' && !$order) {?> selected <?php } ?>>По году (от меньшего к большему)</option>
            <option value="year" data-url="<?php $new_param = $params_url; $new_param['sortby'] = 'year'; $new_param['order'] = 'desc';  echo action('ProductsController@showIndex', $new_param); ?>"<?php if ($sortby == 'year' && $order == 'desc') {?> selected <?php } ?>>По году (от большего к меньшему)</option>
        </select>
    </label>
</div>

<table>
    <thead>
        <td>№</td>
        <td>Название</td>
        <td>Цена</td>
        <td>Описание</td>
        <td>Год</td>
        <td>Дата добавления</td>
    </thead>
    <?php foreach ($products as $item=>$product) { ?>
        <tr>
            <td style="width: 50px"><?php echo $item +1 ; ?></td>
            <td><?php echo ($product->name); ?></td>
            <td>$<?php echo ($product->price); ?></td>
            <td><?php echo ($product->description); ?></td>
            <td><?php echo ($product->year); ?></td>
            <td><?php echo ($product->created_at->toDateTimeString()); ?></td>
        </tr>
    <?php } ?>
</table>
<?php echo $products->links(); ?>
</body>
</html>